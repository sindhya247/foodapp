import 'react-native-gesture-handler';
import React, {useContext} from 'react';
import { Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import SplashScreen from './src/components/SplashScreen';
import {AuthContext, AuthProvider} from './src/components/Context/AuthContext';
import Navigation from './src/components/Navigation/Navigation';


const Stack = createStackNavigator();

export default function App() {


  return (
 <AuthProvider>
         <Navigation />
  </AuthProvider>
  );
}
