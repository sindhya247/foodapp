import { NavigationHelpersContext } from '@react-navigation/native';
import * as React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../../assets/colors/colors';
import styles from './styles';
import popularData from '../../assets/data/popularData';

export default Categories = ({ route, navigation }) => {
  const { item } = route.params;

  const renderIngredientsItem = ({ item }) => {
    return (
      <View
        style={[
          styles.ingredientItemWrapper,
          {
            marginLeft: item.id === '1' ? 20 : 0,
          },
        ]}>
        <Image source={item.image} style={styles.ingredientImage} />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {/* Header */}
      <SafeAreaView>

        <View style={styles.headerWrapper}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <View style={styles.headerLeft}>
              <Feather name="chevron-left" size={12} color={colors.textDark} />
            </View>
          </TouchableOpacity>
          <View style={styles.headerRight}>
            <MaterialCommunityIcons
              name="star"
              size={12}
              color={colors.white}
            />
          </View>
        </View>
      </SafeAreaView>

      <View style={styles.popularWrapper}>
                <Text style={styles.popularTitle}>Popular</Text>
                {popularData.map((item) => (
                  <TouchableOpacity
                    key={item.id}
                    onPress={() =>
                      navigation.navigate('Details', {
                        item: item,
                      })
                    }>
                    <View
                      style={[
                        styles.popularCardWrapper,
                        {
                          marginTop: item.id == 1 ? 10 : 20,
                        },
                      ]}>
                      <View>
                        <View>
                          <View style={styles.popularTopWrapper}>
                            <MaterialCommunityIcons
                              name="crown"
                              size={12}
                              color={colors.primary}
                            />
                            <Text style={styles.popularTopText}>top of the week</Text>
                          </View>
                          <View style={styles.popularTitlesWrapper}>
                            <Text style={styles.popularTitlesTitle}>
                              {item.title}
                            </Text>
                            <Text style={styles.popularTitlesWeight}>
                              Weight {item.weight}
                            </Text>
                          </View>
                        </View>
                        <View style={styles.popularCardBottom}>
                          <View style={styles.addPizzaButton}>
                            <Feather name="plus" size={10} color={colors.textDark} />
                          </View>
                          <View style={styles.ratingWrapper}>
                            <MaterialCommunityIcons
                              name="star"
                              size={10}
                              color={colors.textDark}
                            />
                            <Text style={styles.rating}>{item.rating}</Text>
                          </View>
                        </View>
                      </View>

                      <View style={styles.popularCardRight}>
                        <Image source={item.image} style={styles.popularCardImage} />
                      </View>
                    </View>
                  </TouchableOpacity>
                ))}
              </View>

    </View>
  );
};


