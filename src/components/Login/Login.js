import { Text, View, TextInput, Button, TouchableOpacity, Image, StyleSheet, Alert } from "react-native";
import { useForm, Controller } from "react-hook-form";
import React, {useContext, useState} from 'react';
import Spinner from 'react-native-loading-spinner-overlay';
import {AuthContext} from '../Context/AuthContext';

export default Login = ({navigation}) => {
  const [student_email, setStudent_email] = useState(null);
  const [password, setPassword] = useState(null);
  const {isLoading, loginStudent} = useContext(AuthContext);
return (
 <View style={styles.container}>
   <Spinner visible={isLoading} />
      <Image style={styles.image} source={require("../../assets/images/logo.png")} />

      <View style={styles.inputView}>
   <TextInput
          style={styles.TextInput}
          placeholder="Email."
          placeholderTextColor="#003f5c"
          onChangeText={text => setStudent_email(text)}
          value={student_email}
        />

      </View>
      <View style={styles.inputView}>
    <TextInput
          style={styles.TextInput}
          placeholder="Password."
          placeholderTextColor="#003f5c"
           secureTextEntry={true}
           onChangeText={text => setPassword(text)}
          value={password}
        />

      </View>
      <TouchableOpacity>
        <Text style={styles.forgot_button}>Forgot Password?</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.loginBtn}  onPress={() => {
                                                            loginStudent(student_email, password);
                                                          }}>
        <Text style={styles.loginText} >LOGIN</Text>
      </TouchableOpacity>
    </View>
      );
    }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
  marginTop:20,
     width: 500,
     height: 250,

  },
  inputView: {
    backgroundColor: "#FFC0CB",
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,
    alignItems: "center",
  },
  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
  },
  forgot_button: {
    height: 30,
    marginBottom: 30,
  },
  loginBtn: {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 20,
    backgroundColor: "#FF1493",
  },
});;