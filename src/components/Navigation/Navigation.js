import React, {useContext} from 'react';
import {Text, View} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '../home/Home';
import Details from '../Details';
import Categories from '../Categories/Categories';
import Login from '../Login/Login';
import SplashScreen from '../SplashScreen';
import {AuthContext, AuthProvider} from '../Context/AuthContext';


const Stack = createNativeStackNavigator();

const Navigation = () => {
  const {userInfo, splashLoading} = useContext(AuthContext);

  return (
    <NavigationContainer>
      <Stack.Navigator>
        {splashLoading ? (
          <Stack.Screen
            name="Splash Screen"
            component={SplashScreen}
             options={{headerShown: false}}
          />
        ) : userInfo.length ? (
          <Stack.Screen name="Home" component={Home}    options={{headerShown: false}} />
        ) : (
          <>
            <Stack.Screen
              name="Login"
              component={Login}
              options={{headerShown: false}}
            />

          </>
        )}
         <Stack.Screen
                      name="Categories"
                      component={Categories}
                      options={{headerShown: false}}
                    />
                    <Stack.Screen
                      name="Details"
                      component={Details}
                      options={{headerShown: false}}
                    />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
