import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, {createContext, useContext, useEffect, useState} from 'react';
import {BASE_URL} from '../config';
import { NavigationHelpersContext } from '@react-navigation/native';
import { useNavigation, NavigationContainer, NavigationContext, } from '@react-navigation/native';
export const AuthContext = createContext();


export const AuthProvider = ({children}) => {
 const [userInfo, setUserInfo] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [splashLoading, setSplashLoading] = useState(false);

    const loginStudent = (student_email, password) => {

return(
        axios.post('http://sindhyaedu.com:3001/studentDashboard', {
            email: student_email,
            password: password,
        }).then((res) => {
       let userInfo = res.data;
              setUserInfo(userInfo);
              AsyncStorage.setItem('userInfo', JSON.stringify(userInfo));
              setIsLoading(false);

        })
)
}
 const logout = () => {
    setIsLoading(true);

    axios
      .post('http://sindhyaedu.com:3001/studentDashboard',
        {},
        {
          headers: {Authorization: `Bearer ${userInfo.access_token}`},
        },
      )
      .then(res => {
        console.log(res.data);
        AsyncStorage.removeItem('userInfo');
        setUserInfo({});
        setIsLoading(false);
      })
      .catch(e => {
        console.log(`logout error ${e}`);
        setIsLoading(false);
      });
  };

  const isLoggedIn = async () => {
    try {
      setSplashLoading(true);

      let userInfo = await AsyncStorage.getItem('userInfo');
      userInfo = JSON.parse(userInfo);

      if (userInfo) {
        setUserInfo(userInfo);
      }

      setSplashLoading(false);
    } catch (e) {
      setSplashLoading(false);
      console.log(`is logged in error ${e}`);
    }
  };

  useEffect(() => {
    isLoggedIn();
  }, []);

//console.log(userInfo);
  return (
    <AuthContext.Provider
      value={{
        isLoading,
        userInfo,
        splashLoading,
        loginStudent,
         logout,
      }}>
      {children}
    </AuthContext.Provider>
  );
};
